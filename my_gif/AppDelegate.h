//
//  AppDelegate.h
//  my_gif
//
//  Created by 夏威 on 2020/10/12.
//  Copyright © 2020 yijie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property(nonatomic,strong)UIWindow* window;

@end

