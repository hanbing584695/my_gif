//
//  SceneDelegate.h
//  my_gif
//
//  Created by 夏威 on 2020/10/12.
//  Copyright © 2020 yijie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

